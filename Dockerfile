FROM node:alpine3.14

WORKDIR /nodejs-hello-world
COPY package.json .
COPY . .
CMD node server.js