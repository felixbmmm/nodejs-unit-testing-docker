let assert = require('assert');

let chai = require('chai');
let chaiHttp = require('chai-http')
let server = require('../../server')
let should = chai.should();
let expect = chai.expect;
const sql = require("../../app/models/db.js");

chai.use(chaiHttp);
describe('Customer CRUD', function() {
    before(async () => {
        sql.query("TRUNCATE customers", function(err, res) {
            if (err) {
                throw err;
            }
            console.log('CUSTOMERS table TRUNCATED!');
        });
    });

    /**
     * POST /customers
     */
    describe("POST /customers", function() {
        it('Should call POST /customers and return freshly created customer', (done) => {
            chai.request(server)
                .post('/customers')
                .send({
                    'email': 'test@gmail.com',
                    'name': 'Customer 1',
                    'active': 1
                })
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(201);
                    expect(res.body).to.have.own.property('id');
                    expect(res.body).to.have.own.property('email');
                    expect(res.body).to.have.own.property('name');
                    expect(res.body).to.have.own.property('active');
                    expect(res.body).to.eql({ id: 1, email: 'test@gmail.com', name: 'Customer 1', active: 1 })
    
                    done();
                })
        });
    });

    /**
     * GET /customers
     */
    describe("GET /customers", function() {
        it('Should call GET /customers and return all customers', (done) => {
            let customers = [];
            sql.query("SELECT * FROM customers", (err, res) => {
                if (err) {
                  throw err;
                } else {
                    customers = res;
                }
            });
    
            chai.request(server)
            .get('/customers')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body).to.have.lengthOf(customers.length);
    
                done();
            })
        });
    });

    /**
     * GET /customers/{id}
     */
    describe("GET /customers/{id}", function() {
        it('Should call GET /customers/1 and return customer with the selected id', (done) => {
            chai.request(server)
                .get('/customers/1')
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.own.property('id');
                    expect(res.body).to.have.own.property('email');
                    expect(res.body).to.have.own.property('name');
                    expect(res.body).to.have.own.property('active');
                    expect(res.body).to.eql({ id: 1, email: 'test@gmail.com', name: 'Customer 1', active: 1 })
    
                    done();
                })
        });
    
        it('Should call GET /customers/2 and not return customer with the selected id because it is not found', (done) => {
            chai.request(server)
                .get('/customers/2')
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(404);
                    expect(res.body).to.have.own.property('message').eql('Not found Customer with id 2.');
    
                    done();
                })
        });
    });

    /**
     * PUT /customers/{id}
     */
    describe("PUT /customers/{id}", function() {
        it('Should call PUT /customers/1 and return latest updated customer', (done) => {
            chai.request(server)
                .put('/customers/1')
                .send({
                    'email': 'test@gmail.com',
                    'name': 'Customer 1',
                    'active': 0
                })
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.own.property('id');
                    expect(res.body).to.have.own.property('email');
                    expect(res.body).to.have.own.property('name');
                    expect(res.body).to.have.own.property('active');
                    expect(res.body).to.eql({ id: 1, email: 'test@gmail.com', name: 'Customer 1', active: 0 })
        
                    done();
                })
        });
        
        it('Should call PUT /customers/2 and return error because row not found for selected id', (done) => {
            chai.request(server)
                .put('/customers/2')
                .send({
                    'email': 'test@gmail.com',
                    'name': 'Customer 1',
                    'active': 0
                })
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(404);
        
                    res.body.should.have.property('message').eql('Not found Customer with id 2.');
        
                    done();
                })
        });
    });

    /**
     * DELETE /customers/{id}
     */
    describe("DELETE /customers/{id}", function() {
        it('Should call DELETE /customers/1 and return a success code but with no content', (done) => {
            chai.request(server)
                .delete('/customers/1')
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(204);
    
                    done();
                })
        });
    
        it('Should call DELETE /customers/2 and return error because row not found for selected id', (done) => {
            chai.request(server)
                .delete('/customers/2')
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(404);
    
                    res.body.should.have.property('message').eql('Not found Customer with id 2.');
    
                    done();
                })
        });
    });
});
