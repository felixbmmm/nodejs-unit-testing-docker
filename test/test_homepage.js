let assert = require('assert');

let chai = require('chai');
let chaiHttp = require('chai-http')
let server = require('../server')
let should = chai.should();
let expect = chai.expect;

chai.use(chaiHttp);
describe('/GET index homepage', function() {
  it('Should return Message', (done) => {
    chai.request(server)
        .get('/')
        .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res).to.be.json;

            res.body.should.have.property('message').eql('Hello warlord.');
            done();
        })
  })
});
